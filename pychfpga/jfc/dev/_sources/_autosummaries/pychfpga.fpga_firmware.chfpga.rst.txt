



:py:mod:`chfpga` *module*
=========================

.. currentmodule:: pychfpga.fpga_firmware

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.fpga_firmware,
..    | FULLNAME=pychfpga.fpga_firmware.chfpga,
..    | name==chfpga
..    | modules = ['pychfpga.fpga_firmware.chfpga.SocketIO', 'pychfpga.fpga_firmware.chfpga.chFPGA_receiver', 'pychfpga.fpga_firmware.chfpga.chfpga', 'pychfpga.fpga_firmware.chfpga.ct_engine', 'pychfpga.fpga_firmware.chfpga.f_engine', 'pychfpga.fpga_firmware.chfpga.mmi', 'pychfpga.fpga_firmware.chfpga.system', 'pychfpga.fpga_firmware.chfpga.x_engine']
..    | classes = []

.. automodule:: pychfpga.fpga_firmware.chfpga

   .. *** Insert submodule summary

   
   
   .. container:: autosummary_block

      .. rubric:: Module sub-modules

      .. autosummary::
         :toctree:
         :template: custom-module-template.rst
         :recursive:

         
         ~pychfpga.fpga_firmware.chfpga.SocketIO
         ~pychfpga.fpga_firmware.chfpga.chFPGA_receiver
         ~pychfpga.fpga_firmware.chfpga.chfpga
         ~pychfpga.fpga_firmware.chfpga.ct_engine
         ~pychfpga.fpga_firmware.chfpga.f_engine
         ~pychfpga.fpga_firmware.chfpga.mmi
         ~pychfpga.fpga_firmware.chfpga.system
         ~pychfpga.fpga_firmware.chfpga.x_engine
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions


