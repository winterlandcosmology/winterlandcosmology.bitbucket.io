



:py:mod:`gps` *module*
======================

.. currentmodule:: pychfpga

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga,
..    | FULLNAME=pychfpga.gps,
..    | name==gps
..    | modules = 
..    | classes = ['GPSAsyncRESTClient', 'GPSAsyncRESTServer', 'SpectrumInstrumentsTM4D']

.. automodule:: pychfpga.gps

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module functions summary

      .. autosummary::
         :nosignatures:

         
         main

   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         GPSAsyncRESTClient
         GPSAsyncRESTServer
         SpectrumInstrumentsTM4D
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions


----

.. rubric:: Module functions


.. autofunction:: main





.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: GPSAsyncRESTClient
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: GPSAsyncRESTServer
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: SpectrumInstrumentsTM4D
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


