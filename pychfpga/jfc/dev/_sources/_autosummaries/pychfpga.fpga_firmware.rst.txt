



:py:mod:`fpga_firmware` *module*
================================

.. currentmodule:: pychfpga

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga,
..    | FULLNAME=pychfpga.fpga_firmware,
..    | name==fpga_firmware
..    | modules = ['pychfpga.fpga_firmware.chfpga', 'pychfpga.fpga_firmware.fpga_bitstream', 'pychfpga.fpga_firmware.fpga_firmware']
..    | classes = []

.. automodule:: pychfpga.fpga_firmware

   .. *** Insert submodule summary

   
   
   .. container:: autosummary_block

      .. rubric:: Module sub-modules

      .. autosummary::
         :toctree:
         :template: custom-module-template.rst
         :recursive:

         
         ~pychfpga.fpga_firmware.chfpga
         ~pychfpga.fpga_firmware.fpga_bitstream
         ~pychfpga.fpga_firmware.fpga_firmware
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions


