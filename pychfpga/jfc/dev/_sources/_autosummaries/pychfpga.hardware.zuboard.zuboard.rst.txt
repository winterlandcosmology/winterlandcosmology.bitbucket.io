



:py:mod:`zuboard` *module*
==========================

.. currentmodule:: pychfpga.hardware.zuboard

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.hardware.zuboard,
..    | FULLNAME=pychfpga.hardware.zuboard.zuboard,
..    | name==zuboard
..    | modules = 
..    | classes = ['ZUBoard']

.. automodule:: pychfpga.hardware.zuboard.zuboard

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         ZUBoard
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: ZUBoard
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


