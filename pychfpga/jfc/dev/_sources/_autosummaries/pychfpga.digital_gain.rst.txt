



:py:mod:`digital_gain` *module*
===============================

.. currentmodule:: pychfpga

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga,
..    | FULLNAME=pychfpga.digital_gain,
..    | name==digital_gain
..    | modules = 
..    | classes = ['DigitalGainArchive']

.. automodule:: pychfpga.digital_gain

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         DigitalGainArchive
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: DigitalGainArchive
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


