



:py:mod:`tca6416a` *module*
===========================

.. currentmodule:: pychfpga.hardware.i2c_devices

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.hardware.i2c_devices,
..    | FULLNAME=pychfpga.hardware.i2c_devices.tca6416a,
..    | name==tca6416a
..    | modules = 
..    | classes = ['tca6416a']

.. automodule:: pychfpga.hardware.i2c_devices.tca6416a

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         tca6416a
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: tca6416a
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


