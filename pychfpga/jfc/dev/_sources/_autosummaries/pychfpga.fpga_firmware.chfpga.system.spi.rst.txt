



:py:mod:`spi` *module*
======================

.. currentmodule:: pychfpga.fpga_firmware.chfpga.system

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.fpga_firmware.chfpga.system,
..    | FULLNAME=pychfpga.fpga_firmware.chfpga.system.spi,
..    | name==spi
..    | modules = 
..    | classes = ['SPI']

.. automodule:: pychfpga.fpga_firmware.chfpga.system.spi

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         SPI
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: SPI
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


