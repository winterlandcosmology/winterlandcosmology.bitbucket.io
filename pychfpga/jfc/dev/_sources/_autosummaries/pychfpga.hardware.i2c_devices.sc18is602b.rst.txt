



:py:mod:`sc18is602b` *module*
=============================

.. currentmodule:: pychfpga.hardware.i2c_devices

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.hardware.i2c_devices,
..    | FULLNAME=pychfpga.hardware.i2c_devices.sc18is602b,
..    | name==sc18is602b
..    | modules = 
..    | classes = ['sc18is602b']

.. automodule:: pychfpga.hardware.i2c_devices.sc18is602b

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         sc18is602b
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: sc18is602b
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


