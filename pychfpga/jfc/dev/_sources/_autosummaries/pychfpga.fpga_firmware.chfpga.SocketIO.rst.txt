



:py:mod:`SocketIO` *module*
===========================

.. currentmodule:: pychfpga.fpga_firmware.chfpga

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.fpga_firmware.chfpga,
..    | FULLNAME=pychfpga.fpga_firmware.chfpga.SocketIO,
..    | name==SocketIO
..    | modules = 
..    | classes = ['DataSocket_base']

.. automodule:: pychfpga.fpga_firmware.chfpga.SocketIO

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   .. container:: autosummary_block

      .. rubric:: Module exceptions summary

      .. autosummary::
         :toctree:

         
         FPGAException
   
   


   .. *** Insert function summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module functions summary

      .. autosummary::
         :nosignatures:

         
         get_host_addr

   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         DataSocket_base
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions



----

.. rubric:: Exceptions


.. autoexception:: FPGAException


----


.. *** Insert function descriptions


----

.. rubric:: Module functions


.. autofunction:: get_host_addr





.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: DataSocket_base
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


