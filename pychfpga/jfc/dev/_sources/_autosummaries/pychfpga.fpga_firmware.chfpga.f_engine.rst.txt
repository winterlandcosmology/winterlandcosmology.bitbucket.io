



:py:mod:`f_engine` *module*
===========================

.. currentmodule:: pychfpga.fpga_firmware.chfpga

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.fpga_firmware.chfpga,
..    | FULLNAME=pychfpga.fpga_firmware.chfpga.f_engine,
..    | name==f_engine
..    | modules = ['pychfpga.fpga_firmware.chfpga.f_engine.adcdaq', 'pychfpga.fpga_firmware.chfpga.f_engine.chan', 'pychfpga.fpga_firmware.chfpga.f_engine.fft', 'pychfpga.fpga_firmware.chfpga.f_engine.funcgen', 'pychfpga.fpga_firmware.chfpga.f_engine.prober', 'pychfpga.fpga_firmware.chfpga.f_engine.scaler']
..    | classes = []

.. automodule:: pychfpga.fpga_firmware.chfpga.f_engine

   .. *** Insert submodule summary

   
   
   .. container:: autosummary_block

      .. rubric:: Module sub-modules

      .. autosummary::
         :toctree:
         :template: custom-module-template.rst
         :recursive:

         
         ~pychfpga.fpga_firmware.chfpga.f_engine.adcdaq
         ~pychfpga.fpga_firmware.chfpga.f_engine.chan
         ~pychfpga.fpga_firmware.chfpga.f_engine.fft
         ~pychfpga.fpga_firmware.chfpga.f_engine.funcgen
         ~pychfpga.fpga_firmware.chfpga.f_engine.prober
         ~pychfpga.fpga_firmware.chfpga.f_engine.scaler
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions


