



:py:mod:`amc6821` *module*
==========================

.. currentmodule:: pychfpga.hardware.i2c_devices

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.hardware.i2c_devices,
..    | FULLNAME=pychfpga.hardware.i2c_devices.amc6821,
..    | name==amc6821
..    | modules = 
..    | classes = ['AMC6821']

.. automodule:: pychfpga.hardware.i2c_devices.amc6821

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         AMC6821
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: AMC6821
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


