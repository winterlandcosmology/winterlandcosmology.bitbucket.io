.. pychfpga documentation master file, created by
   sphinx-quickstart on Wed Jan 20 12:29:25 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: html

    <meta http-equiv="refresh" content="0;URL=home.html"/>


.. toctree::
   :hidden:
   :maxdepth: 4
   :caption: Home

   home

.. toctree::
   :hidden:
   :maxdepth: 4
   :caption: Old Home

   Old installation <installation>
   Old setup <howto/setup_python>
   howto/takeData
   howto
   quick_start
   howto/pocket_correlator

.. toctree::
   :maxdepth: 3
   :caption: API Reference
   :hidden:

   _autosummaries/pychfpga

.. toctree::
   :maxdepth: 3
   :caption: External module docs
   :hidden:

   _autosummaries/wtl.metrics
   _autosummaries/wtl.rest

CHIME-specific modules:

.. toctree::
   :maxdepth: 1
   :caption: CHIME-specific docs
   :hidden:

   chime/index





