:mod:`fpga_array` module: FPGA hardware controller
==================================================

.. automodule:: pychfpga.fpga_array

.. currentmodule:: pychfpga.fpga_array

Summary
-------

.. rubric:: Classes

.. autosummary::

      FPGAArray
      FPGABitstream
      .. GPUArray
      PSArray

.. rubric:: Support functions

.. autosummary::

      add_fpga_array_arguments
      add_logging_arguments
      async
      async_return
      create_fpga_array
      load_yaml
      load_yaml_config
      merge_dict

      parse_args_as_dict
      parse_hw_string
      setup_logging
      .. validate_config


Classes
-------

.. autoclass:: FPGAArray

   .. automethod:: __init__


