



:py:mod:`zcu111` *module*
=========================

.. currentmodule:: pychfpga.hardware.zcu111

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.hardware.zcu111,
..    | FULLNAME=pychfpga.hardware.zcu111.zcu111,
..    | name==zcu111
..    | modules = 
..    | classes = ['ZCU111', 'iic_dummy', 'ina226', 'irps5401', 'si5341b', 'si5382', 'si570', 'zynq_sysmon']

.. automodule:: pychfpga.hardware.zcu111.zcu111

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         ZCU111
         iic_dummy
         ina226
         irps5401
         si5341b
         si5382
         si570
         zynq_sysmon
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: ZCU111
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: iic_dummy
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: ina226
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: irps5401
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: si5341b
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: si5382
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: si570
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: zynq_sysmon
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


