



:py:mod:`calculate_gains` *module*
==================================

.. currentmodule:: pychfpga

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga,
..    | FULLNAME=pychfpga.calculate_gains,
..    | name==calculate_gains
..    | modules = 
..    | classes = ['GainCalc']

.. automodule:: pychfpga.calculate_gains

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module functions summary

      .. autosummary::
         :nosignatures:

         
         compute_gains

   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         GainCalc
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions


----

.. rubric:: Module functions


.. autofunction:: compute_gains





.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: GainCalc
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


