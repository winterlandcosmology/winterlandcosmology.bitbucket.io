



:py:mod:`chFPGA_receiver` *module*
==================================

.. currentmodule:: pychfpga.fpga_firmware.chfpga

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.fpga_firmware.chfpga,
..    | FULLNAME=pychfpga.fpga_firmware.chfpga.chFPGA_receiver,
..    | name==chFPGA_receiver
..    | modules = 
..    | classes = ['ReceiverThread', 'chFPGA_receiver']

.. automodule:: pychfpga.fpga_firmware.chfpga.chFPGA_receiver

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         ReceiverThread
         chFPGA_receiver
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: ReceiverThread
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


.. autoclass:: chFPGA_receiver
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


