



:py:mod:`cge` *module*
======================

.. currentmodule:: pychfpga.fpga_firmware.chfpga.ct_engine

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.fpga_firmware.chfpga.ct_engine,
..    | FULLNAME=pychfpga.fpga_firmware.chfpga.ct_engine.cge,
..    | name==cge
..    | modules = 
..    | classes = ['CGE']

.. automodule:: pychfpga.fpga_firmware.chfpga.ct_engine.cge

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         CGE
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: CGE
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


