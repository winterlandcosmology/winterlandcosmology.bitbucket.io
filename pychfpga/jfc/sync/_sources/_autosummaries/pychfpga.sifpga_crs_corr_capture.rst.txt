



:py:mod:`sifpga_crs_corr_capture` *module*
==========================================

.. currentmodule:: pychfpga

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga,
..    | FULLNAME=pychfpga.sifpga_crs_corr_capture,
..    | name==sifpga_crs_corr_capture
..    | modules = 
..    | classes = ['CRS_CORR_CAPTURE']

.. automodule:: pychfpga.sifpga_crs_corr_capture

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         CRS_CORR_CAPTURE
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: CRS_CORR_CAPTURE
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


