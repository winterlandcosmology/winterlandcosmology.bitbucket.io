



:py:mod:`lmx2594spi` *module*
=============================

.. currentmodule:: pychfpga.hardware.i2c_devices

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.hardware.i2c_devices,
..    | FULLNAME=pychfpga.hardware.i2c_devices.lmx2594spi,
..    | name==lmx2594spi
..    | modules = 
..    | classes = ['lmx2594spi']

.. automodule:: pychfpga.hardware.i2c_devices.lmx2594spi

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         lmx2594spi
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: lmx2594spi
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


