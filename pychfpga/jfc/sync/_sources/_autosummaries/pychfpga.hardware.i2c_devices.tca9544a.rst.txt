



:py:mod:`tca9544a` *module*
===========================

.. currentmodule:: pychfpga.hardware.i2c_devices

.. .. note::

..    | template = custom_module-template.rst
..    | Module=pychfpga.hardware.i2c_devices,
..    | FULLNAME=pychfpga.hardware.i2c_devices.tca9544a,
..    | name==tca9544a
..    | modules = 
..    | classes = ['tca9544a']

.. automodule:: pychfpga.hardware.i2c_devices.tca9544a

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         tca9544a
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions




.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: tca9544a
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


