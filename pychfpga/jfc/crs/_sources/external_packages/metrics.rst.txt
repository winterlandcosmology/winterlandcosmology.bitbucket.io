:mod:`metrics`: Simple Prometheus metrics handling
==================================================


.. automodule:: wtl.metrics


.. autoclass: Metrics
	:members:


