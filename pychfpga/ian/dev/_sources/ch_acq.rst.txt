:mod:`pychfpga`
===============

.. image:: images/chime_logo.svg

Python code to operate the CHIME telescope.

.. toctree::

	fpga_master



:mod:`fpga_master` Module
-------------------------

.. automodule:: fpga_master

.. autosummary::

   fpga_master.ChimeMaster

:mod:`pychfpga` Module
-------------------------------

.. automodule:: pychfpga

.. autosummary::

	pychfpga.calculate_gains
	pychfpga.fpga_array
	pychfpga.digital_gain





.. .. automethod:: chFPGA_controller.__init__(*see below*)
