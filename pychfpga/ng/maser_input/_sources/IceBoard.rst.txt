pychfpga.core.icecore.IceBoard
==============================

.. currentmodule:: pychfpga.core.icecore

.. autoclass:: IceBoard

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~IceBoard.__init__
      ~IceBoard.self_getter
      ~IceBoard.update_handler
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~IceBoard.crate
      ~IceBoard.handler
      ~IceBoard.handler_id
      ~IceBoard.handler_name
      ~IceBoard.hostname
      ~IceBoard.hwm
      ~IceBoard.metadata
      ~IceBoard.mezzanine
      ~IceBoard.mezzanines
      ~IceBoard.serial
      ~IceBoard.slot
   
   