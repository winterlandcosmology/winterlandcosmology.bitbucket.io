



:py:mod:`old_hp8753e` *module*
==============================

.. currentmodule:: labpy.instruments.hp

.. .. note::

..    | template = custom_module-template.rst
..    | Module=labpy.instruments.hp,
..    | FULLNAME=labpy.instruments.hp.old_hp8753e,
..    | name==old_hp8753e
..    | modules = 
..    | classes = ['GPIB']

.. automodule:: labpy.instruments.hp.old_hp8753e

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module functions summary

      .. autosummary::
         :nosignatures:

         
         doSparam
         loadTxtData
         parseData
         plotGammaDataRL
         plotNetworkData
         vna_sweep

   
   

   .. *** Insert class summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module classes summary

      .. autosummary::
         :nosignatures:

         
         GPIB
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions


----

.. rubric:: Module functions


.. autofunction:: doSparam


.. autofunction:: loadTxtData


.. autofunction:: parseData


.. autofunction:: plotGammaDataRL


.. autofunction:: plotNetworkData


.. autofunction:: vna_sweep





.. *** Insert class descriptions



----

.. rubric:: Module classes


.. autoclass:: GPIB
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__, __init__


