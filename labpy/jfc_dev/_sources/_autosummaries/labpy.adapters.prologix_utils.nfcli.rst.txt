



:py:mod:`nfcli` *module*
========================

.. currentmodule:: labpy.adapters.prologix_utils

.. .. note::

..    | template = custom_module-template.rst
..    | Module=labpy.adapters.prologix_utils,
..    | FULLNAME=labpy.adapters.prologix_utils.nfcli,
..    | name==nfcli
..    | modules = 
..    | classes = []

.. automodule:: labpy.adapters.prologix_utils.nfcli

   .. *** Insert submodule summary

   
   
   

   .. *** Insert attribute summary

   
   
   

   .. *** Insert exception summary

   
   
   


   .. *** Insert function summary

   
   

   .. container:: autosummary_block

      .. rubric:: Module functions summary

      .. autosummary::
         :nosignatures:

         
         ValidateNetParams
         enumIp
         enumIpUnix
         main
         usage

   
   

   .. *** Insert class summary

   
   
   



.. *** Insert attribute descriptions





.. *** Insert exceptions descriptions




.. *** Insert function descriptions


----

.. rubric:: Module functions


.. autofunction:: ValidateNetParams


.. autofunction:: enumIp


.. autofunction:: enumIpUnix


.. autofunction:: main


.. autofunction:: usage





.. *** Insert class descriptions


