:entity:`ADCDAQ_FIFO`: FIFO to cross clock phase boundaries and allow line delay compensation
*********************************************************************************************

.. image:: ../images/xilinx_modules/adcdaq_fifo.png
    :width: 25%

Basic
=====

.. image:: ../images/xilinx_modules/adcdaq_fifo1.png
    :width: 100%

Native Ports
============

.. image:: ../images/xilinx_modules/adcdaq_fifo2.png
	:width: 100%

Status Flags
============

.. image:: ../images/xilinx_modules/adcdaq_fifo3.png
	:width: 100%

Summary
=======

.. image:: ../images/xilinx_modules/adcdaq_fifo4.png
	:width: 100%