.. _xglink-rx-data-fifo-xil-module:

Xglink Rx Data FIFO
*******************

.. image:: ../images/xilinx_modules/xglink_rx_data_fifo.png
    :width: 25%

Basic
=====

.. image:: ../images/xilinx_modules/xglink_rx_data_fifo1.png
    :width: 100%

AXI4 Stream Ports
=================

.. image:: ../images/xilinx_modules/xglink_rx_data_fifo2.png
	:width: 100%

Config
======

.. image:: ../images/xilinx_modules/xglink_rx_data_fifo3.png
	:width: 100%

Status Flags
============

.. image:: ../images/xilinx_modules/xglink_rx_data_fifo4.png
	:width: 100%

Summary
=======

.. image:: ../images/xilinx_modules/xglink_rx_data_fifo5.png
	:width: 100%