.. _udp-mac-tx-data-fifo-xil-module:

UDP Mac Tx Data FIFO
********************

.. image:: ../images/xilinx_modules/udp_mac_tx_data_fifo.png
    :width: 25%

Basic
=====

.. image:: ../images/xilinx_modules/udp_mac_tx_data_fifo1.png
    :width: 100%

Native Ports
============

.. image:: ../images/xilinx_modules/udp_mac_tx_data_fifo2.png
	:width: 100%

Status Flags
============

.. image:: ../images/xilinx_modules/udp_mac_tx_data_fifo3.png
	:width: 100%

Data Counts
===========

.. image:: ../images/xilinx_modules/udp_mac_tx_data_fifo4.png
	:width: 100%

Summary
=======

.. image:: ../images/xilinx_modules/udp_mac_tx_data_fifo5.png
	:width: 100%