.. _shuffle-bin-sel-flags-fifo-xil-module:

Shuffle Bin Selector Flags FIFO
*******************************

.. image:: ../images/xilinx_modules/shuffle_bin_sel_flags_fifo.png
    :width: 25%

Basic
=====

.. image:: ../images/xilinx_modules/shuffle_bin_sel_flags_fifo1.png
    :width: 100%

Native Ports
============

.. image:: ../images/xilinx_modules/shuffle_bin_sel_flags_fifo2.png
	:width: 100%

Status Flags
============

.. image:: ../images/xilinx_modules/shuffle_bin_sel_flags_fifo3.png
	:width: 100%

Data Counts
===========

.. image:: ../images/xilinx_modules/shuffle_bin_sel_flags_fifo4.png
	:width: 100%

Summary
=======

.. image:: ../images/xilinx_modules/shuffle_bin_sel_flags_fifo5.png
	:width: 100%