.. _shuffle_crossbar-align-fifo-xil-module:

Shuffle Crossbar Align FIFO
***************************

.. image:: ../images/xilinx_modules/shuffle_crossbar_align_fifo.png
    :width: 25%

Basic
=====

.. image:: ../images/xilinx_modules/shuffle_crossbar_align_fifo1.png
    :width: 100%

Native Ports
============

.. image:: ../images/xilinx_modules/shuffle_crossbar_align_fifo2.png
	:width: 100%

Status Flags
============

.. image:: ../images/xilinx_modules/shuffle_crossbar_align_fifo3.png
	:width: 100%

Data Counts
===========

.. image:: ../images/xilinx_modules/shuffle_crossbar_align_fifo4.png
	:width: 100%

Summary
=======

.. image:: ../images/xilinx_modules/shuffle_crossbar_align_fifo5.png
	:width: 100%