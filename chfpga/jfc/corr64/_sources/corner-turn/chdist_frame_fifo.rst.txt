.. _chdist-frame-fifo-xil-module:

CHDIST Frame FIFO
*****************

.. image:: ../images/xilinx_modules/chdist_frame_fifo.png
    :width: 25%

Basic
=====

.. image:: ../images/xilinx_modules/chdist_frame_fifo1.png
	:width: 100%

Native Ports
============

.. image:: ../images/xilinx_modules/chdist_frame_fifo2.png
	:width: 100%

Status Flags
============

.. image:: ../images/xilinx_modules/chdist_frame_fifo3.png
	:width: 100%

Summary
=======

.. image:: ../images/xilinx_modules/chdist_frame_fifo4.png
	:width: 100%