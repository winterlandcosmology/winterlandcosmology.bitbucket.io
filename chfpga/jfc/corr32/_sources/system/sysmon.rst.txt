..   .. title:: SYSMON_XADC

***************************************
:entity:`SYSMON_XADC` (*SYSMON_V6.vhd*)
***************************************


.. .. figure:: ../images/SYSMON.svg
..     :width: 100%

Entity documentation
====================

.. parse:: hdl/SYSTEM/SYSMON_V6.vhd

.. autoentity:: SYSMON_XADC




Additional details
==================


Memory map
==========

.. include-docs:: SYSMON_XADC
    :start-after: Memory map

