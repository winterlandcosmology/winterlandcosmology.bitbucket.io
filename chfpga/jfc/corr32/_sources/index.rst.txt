.. Redirect immediately to the home page
.. raw:: html

    <meta http-equiv="refresh" content="0;URL=introduction.html"/>

.. title:: ``chFPGA`` documentation


.. toctree::
   :maxdepth: 5
   :caption: Getting started
   :hidden:

   introduction
   firmware_architecture
   source_code_information
.. requirements
.. implementation
.. internal_buses
.. udp_command_protocol
.. modules
.. clocking
.. channelizer
.. crossbar
.. backplane_shuffle

.. toctree::
   :maxdepth: 5
   :caption: VHDL Source code
   :hidden:
   :includehidden:

   platforms/platforms
   chfpga
   system/system
   channelizer/index
   CT Engine <corner-turn/corner-turn>
   X Engine <correlator/correlator>

.. toctree::
   :maxdepth: 1
   :caption: Indices and Tables
   :hidden:

   genindex
   modindex

